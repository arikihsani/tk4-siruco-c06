from django import forms

class RuanganHotelForm(forms.Form):
    kode_hotel = forms.CharField(max_length=5, required=False) 
    kode_ruangan = forms.CharField(max_length=5, required=False)
    jenis_bed = forms.CharField(max_length=10, required=False)
    tipe = forms.CharField(max_length=10, required=False)
    harga_per_harian = forms.CharField(max_length=20, required=False)

class ReservasiHotelForm(forms.Form):
    tanggal_masuk = forms.DateField(required=False) 
    tanggal_keluar = forms.DateField(required=False)

class UpdateTransaksiHotelForm(forms.Form):
    statusbayar = forms.CharField(max_length=15, required=False) 

    
