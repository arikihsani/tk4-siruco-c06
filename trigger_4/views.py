from django.shortcuts import render, reverse, redirect
from .forms import *
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.db import IntegrityError, InternalError
from json import dumps
import datetime

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def buat_ruangan(request):
    pesan = ''
    if request.method == "POST": 
        formulir = RuanganHotelForm(request.POST) 

        kode_hotel = str(request.POST['kode_hotel'])
        jenis_bed = str(request.POST['jenis_bed'])
        tipe = str(request.POST['tipe'])
        harga_per_harian = str(request.POST['harga_per_harian'])
        if kode_hotel == '' or jenis_bed == '' or tipe == '' or harga_per_harian == '':
            pesan = 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu'
        else :
            cursor = connection.cursor()
            cursor.execute("set search_path to siruco")
            #cursor.execute("select Concat('RH',to_char((Substring(MAX(koderoom),3)::integer + 1) ,'FM009')) from hotel_room where koderoom like 'RH%';")

            cursor.execute("select Concat(kodehotel,to_char((Substring(MAX(koderoom),4)::integer + 1) ,'FM09')) from hotel_room where kodehotel = '"+kode_hotel+"' group by kodehotel;")

            hasil1 = namedtuplefetchall(cursor)
            kode_ruangan = hasil1[0].concat

            cursor.execute("insert into hotel_room values ('"+kode_hotel+ "','" +kode_ruangan+"', '"+jenis_bed+"', '"+tipe+"', '"+harga_per_harian+ "')") 
            cursor.execute("set search_path to public")
            cursor.close()
        
            return redirect('trigger_4:list-ruangan-hotel')

    formulir = RuanganHotelForm()
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from hotel") 
    hasil = namedtuplefetchall(cursor)
    kode_hotel = []
    for x in range (len(hasil)):
        kode_hotel.append(hasil[x].kode)

    cursor.execute("select Concat('RH',to_char((Substring(MAX(koderoom),3)::integer + 1) ,'FM009')) from hotel_room where koderoom like 'RH%';")
    hasil1 = namedtuplefetchall(cursor)
    kode_ruangan = hasil1[0].concat
    cursor.execute("set search_path to public")
    cursor.close()
    argument = {    
        'pendaftaranRuanganHotel' : formulir,
        'kode_hotel' : kode_hotel,
        'kode_ruangan' : kode_ruangan,
        'pesan' : pesan,
    }
    return render(request, 'pendaftaran-ruangan-hotel.html', argument)



def list_ruangan(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from hotel_room")
    hasil = namedtuplefetchall(cursor)
    cursor.execute("select * from reservasi_hotel")
    hasil1 = namedtuplefetchall(cursor)
     
    cursor.execute("select kodehotel, max(tglkeluar) from reservasi_hotel group by kodehotel;")
    hasil2 = namedtuplefetchall(cursor)
    dict_tglkeluar= {}
    for item in hasil2:
        dict_tglkeluar[item.kodehotel] = item.max
   
    cursor.execute("set search_path to public")
    cursor.close()
    date = datetime.date.today() 

    reservasi = []
    for x in range (len(hasil1)):
            reservasi.append(hasil1[x].koderoom)
    # print (reservasi)
    argument = {
        'table' : hasil,
        'reservasi' : reservasi,
        'date' : date,
        'dict_tglkeluar' : dict_tglkeluar,
    }
    return render(request, 'list-hotel-room.html', argument)

def update_ruangan(request, kodehotel, koderoom):
    pesan =''
    if request.method == "POST":
         
        jenis_bed = str(request.POST['jenis_bed']) 
        tipe = str(request.POST['tipe']) 
        harga_per_harian = str(request.POST['harga_per_harian'])
        if jenis_bed == '' or tipe == '' or harga_per_harian == '':
            pesan = 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu'
        else : 
            with connection.cursor() as c:
                c.execute("""UPDATE SIRUCO.hotel_room SET jenisbed =  %s, tipe =  %s, 
                            harga = %s WHERE kodehotel = %s and  koderoom = %s""",
                            [jenis_bed, tipe, harga_per_harian, kodehotel, koderoom]
                )
        
            
            return HttpResponseRedirect(reverse('trigger_4:list-ruangan-hotel'))

    
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from hotel_room where kodehotel = '" +kodehotel+"' and koderoom = '" +koderoom+"'")
    hasil = namedtuplefetchall(cursor)
    hotelroom = hasil[0]
    cursor.execute("set search_path to public")
    cursor.close()

    data_awal = {
    'kode_hotel': hotelroom[0],
    'kode_ruangan': hotelroom[1],
    'jenis_bed': hotelroom[2],
    'tipe': hotelroom[3],
    'harga_per_harian': hotelroom[4]
    }
    form_update_hotel_room = RuanganHotelForm(initial = data_awal)

    argument = {    
    'pendaftaranRuanganHotel' : form_update_hotel_room,
    'hotelroom' : hotelroom,
    'pesan' : pesan,
    }
    return render(request, 'update-hotel-room.html', argument)

def delete_ruangan(request, kodehotel, koderoom):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("delete from hotel_room where kodehotel = '" +kodehotel+"' and koderoom = '"+koderoom+"'")
    cursor.execute("set search_path to public")
    cursor.close()
    return redirect('trigger_4:list-ruangan-hotel')

def list_ruangan_viewonly(request):
    email = request.session['email']
    password = request.session['password']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from hotel_room")
    hasil = namedtuplefetchall(cursor)

   
    cursor.execute("select * from akun_pengguna where username='"+email+"' and password='"+password+"'")
    hasil1 = namedtuplefetchall(cursor)
    peran = hasil1[0].peran
    cursor.execute("set search_path to public")
    cursor.close()

    argument = {
        'table' : hasil,
    }

    if (peran == "Admin_satgas"):
        return render(request, 'list-hotel-room-viewonly-satgas.html', argument)
    elif (peran == "Pengguna_publik"):
        return render(request, 'list-hotel-room-viewonly-publik.html', argument)
    

def buat_reservasi_hotel(request):
    pesan = ''
    if request.method == "POST": 
        formulir = RuanganHotelForm(request.POST) 
        nik_pasien = str(request.POST['nik_pasien'])
        tanggal_masuk = str(request.POST['tanggal_masuk'])
        tanggal_keluar = str(request.POST['tanggal_keluar'])
        kode_hotel = str(request.POST['kode_hotel'])
        kode_ruangan = str(request.POST['kode_ruangan'])
        if nik_pasien == '' or tanggal_masuk == '' or tanggal_keluar == '' or kode_hotel == '' or kode_ruangan == '':
            pesan = 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu'
        else :
            cursor = connection.cursor()
            cursor.execute("set search_path to siruco")
            cursor.execute("insert into reservasi_hotel values ('"+nik_pasien+ "','" +tanggal_masuk+"', '"+tanggal_keluar+"', '"+kode_hotel+"', '"+kode_ruangan+ "')") 
            cursor.execute("set search_path to public")
            cursor.close()
        
            return redirect('trigger_4:list-reservasi-hotel')

    
    formulir = ReservasiHotelForm() 
    email = request.session['email']
    password = request.session['password']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")

    cursor.execute("select * from akun_pengguna where username='"+email+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)
    peran = hasil[0].peran

    cursor.execute("select * from pasien") 
    hasil = namedtuplefetchall(cursor)
    list_nik = []
    for x in range (len(hasil)):
        list_nik.append(hasil[x].nik)
    
    
    cursor.execute("select * from hotel") 
    hasil = namedtuplefetchall(cursor)
    list_hotel = []
    for x in range (len(hasil)):
        list_hotel.append(hasil[x].kode)

    cursor.execute("select * from pasien where idpendaftar = '"+email+"'") 
    hasil = namedtuplefetchall(cursor)
    list_nik_publik = []
    for x in range (len(hasil)):
        list_nik_publik.append(hasil[x].nik)


    dict_kodehotel = {}
    cursor.execute("select distinct kodehotel from hotel_room") 
    kode_hotel = namedtuplefetchall(cursor)
    
    for kode in kode_hotel:
        list_temp = []
        cursor.execute("select koderoom from hotel_room where kodehotel = '"+kode.kodehotel+"'")
        list_koderoom = namedtuplefetchall(cursor)
        for koderoom in list_koderoom:
            list_temp.append(koderoom.koderoom)
        
        dict_kodehotel[kode.kodehotel] = list_temp
    
    dataJSON = dumps(dict_kodehotel)

    cursor.execute("set search_path to public")
    cursor.close()
    
    argument = {    
        'ReservasiHotel' : formulir,
        'list_nik' : list_nik,
        'list_hotel' : list_hotel,
        'list_nik_publik' : list_nik_publik,
        'dict_kodehotel' : dict_kodehotel,
        'data' : dataJSON,
        'pesan' : pesan,
    }

    if (peran == 'Admin_satgas'):
            return render(request, 'buat-reservasi-hotel.html', argument)
    elif (peran == 'Pengguna_publik'):
            return render(request, 'buat-reservasi-hotel-publik.html', argument)
   
def list_reservasi(request):
    email = request.session['email']
    password = request.session['password']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from akun_pengguna where username='"+email+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)
    peran = hasil[0].peran
    cursor.execute("select * from reservasi_hotel")
    hasil = namedtuplefetchall(cursor)
    date = datetime.date.today()
    
    print(date)
    print(date < hasil[0].tglmasuk)
    argument = {
        'table' : hasil,
        'date' : date,
    }
    cursor.execute("set search_path to public")
    cursor.close()
    if (peran == 'Admin_satgas'):
             return render(request, 'list-reservasi-hotel.html', argument)
    elif (peran == 'Pengguna_publik'):
             return render(request, 'list-reservasi-hotel-publik.html', argument)
    
def delete_reservasi(request, kodepasien, tglmasuk):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco") 
    cursor.execute("select * from transaksi_booking where kodepasien= '" +kodepasien+"' and tglmasuk = '"+tglmasuk+"'")
    hasil = namedtuplefetchall(cursor)
    idtransaksi = hasil[0].idtransaksibooking
    cursor.execute("delete from reservasi_hotel where kodepasien= '" +kodepasien+"' and tglmasuk = '"+tglmasuk+"'")
    cursor.execute("delete from transaksi_hotel where idtransaksi = '" +idtransaksi+"'")
    cursor.execute("set search_path to public")
    cursor.close()
    return redirect('trigger_4:list-reservasi-hotel')

def update_reservasi(request, kodepasien, tglmasuk):
    pesan = ''
    if request.method == "POST":
         
        tglkeluar = str(request.POST['tanggal_keluar'])
        if tglkeluar == '':
            pesan = 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu'
        else : 
        
            with connection.cursor() as c:
                c.execute("""UPDATE SIRUCO.reservasi_hotel SET tglkeluar =  %s WHERE kodepasien = %s and  tglmasuk = %s""",
                            [tglkeluar, kodepasien, tglmasuk]
                )
            
                
            return HttpResponseRedirect(reverse('trigger_4:list-reservasi-hotel'))

    
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from reservasi_hotel where kodepasien = '" +kodepasien+"' and tglmasuk = '" +tglmasuk+"'")
    hasil = namedtuplefetchall(cursor)
    reservasihotel = hasil[0]
    cursor.execute("set search_path to public")
    cursor.close()

    data_awal = {
    'tanggal_keluar': reservasihotel[2],
    }
    form_update_reservasi_hotel = ReservasiHotelForm(initial = data_awal)

    argument = {    
    'Formupdatereservasi' : form_update_reservasi_hotel,
    'Reservasi' : reservasihotel,
    'pesan' : pesan,
    }
    return render(request, 'update-resrvasi-hotel.html', argument)

def list_transaksi_hotel(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from transaksi_hotel")
    hasil = namedtuplefetchall(cursor)
    
    argument = {
        'transaksi_hotel' : hasil,
    }
    cursor.execute("set search_path to public")
    cursor.close()
    return render(request, 'list-transaksi-hotel.html', argument)


def update_transaksi_hotel (request, idtransaksi):
    pesan =''
    if request.method == "POST":
         
        statusbayar = str(request.POST['statusbayar'])
        if statusbayar == '':
            pesan = 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu'
        else :
            with connection.cursor() as c:
                c.execute("""UPDATE SIRUCO.transaksi_hotel SET statusbayar =  %s WHERE idtransaksi = %s """,
                            [statusbayar, idtransaksi]
                )
            
                
            return HttpResponseRedirect(reverse('trigger_4:list-transaksi-hotel'))

    
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from transaksi_hotel where idtransaksi = '" +idtransaksi+"'")
    hasil = namedtuplefetchall(cursor)
    transaksihotel = hasil[0]
    cursor.execute("set search_path to public")
    cursor.close()

    data_awal = {
    'statusbayar': transaksihotel[5],
    }
    form_update_transaksi_hotel = UpdateTransaksiHotelForm(initial = data_awal)

    argument = {    
    'Formupdatetransaksi' : form_update_transaksi_hotel,
    'Transaksihotel' : transaksihotel,
    'pesan' : pesan,
    }
    return render(request, 'update-transaksi-hotel.html', argument)

def list_transaksi_booking_hotel (request):
    email = request.session['email']
    password = request.session['password']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from akun_pengguna where username='"+email+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)
    peran = hasil[0].peran
    cursor.execute("select * from transaksi_booking")
    hasil1 = namedtuplefetchall(cursor)

    argument = {
        'transaksi_booking' : hasil1,
    }
    cursor.execute("set search_path to public")
    cursor.close()

    if (peran == 'Admin_satgas'):
             return render(request, 'list-transaksi-booking-hotel-satgas.html', argument)
    elif (peran == 'Pengguna_publik'):
             return render(request, 'list-transaksi-booking-hotel-publik.html', argument)