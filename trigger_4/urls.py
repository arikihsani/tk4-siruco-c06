from django.urls import path

from . import views

app_name = 'trigger_4'

urlpatterns = [
    path('pendaftaran-ruangan-hotel/', views.buat_ruangan, name='pendaftaran-ruangan-hotel'),
    path('list-ruangan-hotel/', views.list_ruangan, name='list-ruangan-hotel'),
    path('update-ruangan-hotel/<str:kodehotel>/<str:koderoom>', views.update_ruangan, name='update-ruangan-hotel'),
    path('delete-ruangan-hotel/<str:kodehotel>/<str:koderoom>', views.delete_ruangan, name='delete-ruangan-hotel'),
    path('list-ruangan-hotel-viewonly/', views.list_ruangan_viewonly, name='list-ruangan-hotel-viewonly'),
    path('buat-reservasi-hotel/', views.buat_reservasi_hotel, name='buat-reservasi-hotel'),
    path('list-reservasi-hotel/', views.list_reservasi, name='list-reservasi-hotel'),
    path('delete-reservasi-hotel/<str:kodepasien>/<str:tglmasuk>', views.delete_reservasi, name='delete-reservasi-hotel'),
    path('update-reservasi-hotel/<str:kodepasien>/<str:tglmasuk>', views.update_reservasi, name='update-reservasi-hotel'),
    path('list-transaksi-hotel/', views.list_transaksi_hotel, name='list-transaksi-hotel'),
    path('update-transaksi-hotel/<str:idtransaksi>', views.update_transaksi_hotel, name='update-transaksi-hotel'),
     path('list-transaksi-booking-hotel/', views.list_transaksi_booking_hotel, name='list-transaksi-booking-hotel'),
]
