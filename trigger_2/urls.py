from django.urls import path

from . import views

app_name = 'trigger_2'

urlpatterns = [
    path('lihat-jadwal-dokter/', views.lihat_jadwal_dokter, name='lihat_jadwal_dokter'),
    path('list-buat-jadwal-dokter/', views.list_buat_jadwal_dokter, name='list_buat_jadwal_dokter'),
    path('buat-jadwal/<str:kode_faskes>/<str:shift>/<str:tanggal>', views.buat_jadwal_dokter, name='buat_jadwal_dokter'),

    path('list-create-appointment/', views.list_create_appointment, name='list_create_appointment'),
    path('create-appointment/<str:nostr>/<str:username>/<str:kode_faskes>/<str:tgl_praktek>/<str:shift_praktek>', views.create_appointment, name='create_appointment'),
    path('read-appointment/', views.read_appointment, name='read_appointment'),
    path('delete-appointment/<str:nik>/<str:email_dokter>/<str:shift_praktek>/<str:tgl_praktek>/<str:kode_faskes>', views.delete_appointment, name='delete_appointment'),
    path('update-appointment/<str:nik>/<str:email_dokter>/<str:shift_praktek>/<str:tgl_praktek>/<str:kode_faskes>', views.update_appointment, name='update_appointment'),

    path('create-ruangan-rs/', views.create_ruangan_rs, name='create_ruangan_rs'),
    path('list-ruangan-rs/', views.list_ruangan_rs, name='list_ruangan_rs'),
    path('update-ruangan-rs/<str:koders>/<str:koderuangan>/<str:tipe>/<int:harga>', views.update_ruangan_rs, name='update_ruangan_rs'),
    path('create-bed-rs/', views.create_bed_rs, name='create_bed_rs'),
    path('list-bed-rs/', views.list_bed_rs, name='list_bed_rs'),
    path('delete-bed-rs/<str:koderuangan>/<str:koders>/<str:kodebed>', views.delete_bed_rs, name='delete_bed_rs'),
]
