from django import forms

class UpdateRekomendasi(forms.Form):
    rekomendasi = forms.CharField(max_length=500,widget=forms.Textarea, required=False) 

class CreateRuanganRS(forms.Form):
    tipe = forms.CharField(max_length=10, required=False)
    harga = forms.IntegerField(required=False)
    kode_ruangan = forms.CharField(max_length=5, required=False)

class UpdateRuanganRS(forms.Form):
    tipe = forms.CharField(max_length=10, required=False) 
    harga = forms.IntegerField(required=True) 
