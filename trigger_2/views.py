from django.shortcuts import render, reverse, redirect
from .forms import *
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect, JsonResponse
from django.db import IntegrityError, InternalError
from json import dumps
import datetime

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

# Fitur nomor 4

def check_peran(email):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from akun_pengguna where username='"+email+"'")
    hasil = namedtuplefetchall(cursor)
    peran = hasil[0].peran
    cursor.execute("set search_path to public")
    cursor.close()
    return peran

def check_base(email):
    peran = check_peran(email)
    base_html = ''
    if(peran == 'Pengguna_publik'):
        return 'base-pengguna-publik.html'
    elif(peran == 'Admin_satgas'):
        return 'base-satgas.html'

def lihat_jadwal_dokter(request):
    username = request.session['email']
    peran = check_peran(username)
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    if(peran == 'Dokter'):
        cursor.execute("select * from jadwal_dokter where username = '" + username +"'")
        hasil = namedtuplefetchall(cursor)
        cursor.execute("set search_path to public")
        argument = {
            'table':hasil,
        }
        cursor.close()
        return render(request, 'trigger_2/lihat-jadwal-viewonly-dokter.html', argument)

    elif(peran == 'Admin_satgas'):
        cursor.execute("select * from jadwal_dokter")
        hasil = namedtuplefetchall(cursor)
        argument = {
            'table':hasil,
        }
        cursor.close()
        return render(request, 'trigger_2/lihat-jadwal-viewonly-satgas.html', argument)

    elif(peran == 'Pengguna_publik'):
        cursor.execute("select * from jadwal_dokter")
        hasil = namedtuplefetchall(cursor)
        argument = {
            'table':hasil,
        }
        cursor.close()
        return render(request, 'trigger_2/lihat-jadwal-viewonly-pengguna.html', argument)

def list_buat_jadwal_dokter(request):
    dokter = request.session['email']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("(select * from jadwal) except (select kode_faskes, shift, tanggal from jadwal_dokter where username = '" + dokter +"')")
    hasil = namedtuplefetchall(cursor)
    cursor.execute("set search_path to public")
    argument = {
        'table':hasil,
    }
    cursor.close()
    return render(request, 'trigger_2/buat-jadwal.html', argument)

def buat_jadwal_dokter(request, kode_faskes, shift, tanggal):
    dokter = request.session['email']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from dokter where username ='"+dokter+ "'")
    data_dokter = namedtuplefetchall(cursor)
    nostr = data_dokter[0].nostr
    
    cursor.execute("insert into jadwal_dokter values('"+ nostr +"','"+ dokter +"','"+ kode_faskes +"','"+ shift +"','"+ tanggal + "', 0)")
    cursor.execute("set search_path to public")
    cursor.close()
    return redirect('trigger_2:lihat_jadwal_dokter')

# Fitur nomor 5

def list_create_appointment(request, pesan=''):
    username = request.session['email']
    base_html = check_base(username)
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from jadwal_dokter")
    jadwal_dokter = namedtuplefetchall(cursor)

    argument = {
        'base_html': base_html,
        'jadwal_dokter': jadwal_dokter,
        'pesan': pesan,
    }
 
    cursor.execute("set search_path to public")
    cursor.close()
    return render(request, 'trigger_2/list-create-appointment.html', argument)

def create_appointment(request, nostr, username, kode_faskes, tgl_praktek, shift_praktek):
    if request.method == "POST":
        nik = str(request.POST['nik_pasien'])
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        try:
            cursor.execute("insert into memeriksa values('"+ nik +"','"+ nostr +"','"+ username + "','" + kode_faskes +"','"+ shift_praktek +"','"+ tgl_praktek + "')")
            cursor.execute("set search_path to public")
            cursor.close()
            return redirect('trigger_2:list_create_appointment') # NANTI DIGANTI REDIRECT KE LIST APPOINTMENT
        except InternalError:
            print("masuk sini ga")
            pesan = 'Silahkan pilih shift dan tanggal lainnya, karena shift dan tanggal yang dipilih sudah penuh'
            cursor.execute("set search_path to public")
            cursor.close()
            return list_create_appointment(request, pesan)

    email = request.session['email']
    base_html = check_base(email)
    list_nik = []
    peran = check_peran(email)
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    
    # disini saya tambahin constraint, kalau kombinasi: nik, nostr, username, kode_faskes, tgl_praktek, shift_praktek sudah ada di memeriksa 
    # maka tidak akan ditampilkan pada dropdown nik.
    # karena kalau tidak ditambahkan constraint ini, jika pasien tsb sudah dibuatkan appointment sesuai appointment yang dipilih maka akan error.

    if(peran == 'Pengguna_publik'):
        cursor.execute("select * from pasien where idpendaftar='"+email+"' AND nik not in (select nik_pasien from memeriksa where nostr='"+ nostr +"' AND username_dokter='"+ username+"' AND kode_faskes = '"+ kode_faskes +"' AND praktek_shift = '"+ shift_praktek +"' AND praktek_tgl='"+ tgl_praktek +"')") 
        hasil = namedtuplefetchall(cursor)
        for x in range (len(hasil)):
            list_nik.append(hasil[x].nik)

    elif(peran == 'Admin_satgas'):
        cursor.execute("select * from pasien where nik not in (select nik_pasien from memeriksa where nostr='"+ nostr +"' AND username_dokter='"+ username+"' AND kode_faskes = '"+ kode_faskes +"' AND praktek_shift = '"+ shift_praktek +"' AND praktek_tgl='"+ tgl_praktek +"')") 
        hasil = namedtuplefetchall(cursor)
        for x in range (len(hasil)):
            list_nik.append(hasil[x].nik)

    argument = {
        'base_html': base_html,
        'list_nik': list_nik,
        'username': username,
        'kode_faskes': kode_faskes,
        'tgl_praktek': tgl_praktek,
        'shift_praktek': shift_praktek,
    }
    cursor.execute("set search_path to public")
    cursor.close()
    return render(request, 'trigger_2/create-appointment.html', argument)

def read_appointment(request, pesan=''):
    email = request.session['email']
    peran = check_peran(email)
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    if(peran == 'Pengguna_publik'):
        cursor.execute("select * from memeriksa where nik_pasien in (select nik from pasien where idpendaftar ='"+ email +"')")
        memeriksa = namedtuplefetchall(cursor)
        argument = {
            'memeriksa':memeriksa,
        }
        cursor.execute("set search_path to public")
        cursor.close()
        return render(request, 'trigger_2/read-appointment-viewonly-pengguna.html', argument)

    elif(peran == 'Admin_satgas'):
        cursor.execute("select * from memeriksa")
        memeriksa = namedtuplefetchall(cursor)
        argument = {
            'memeriksa':memeriksa,
        }
        cursor.execute("set search_path to public")
        cursor.close()
        return render(request, 'trigger_2/read-appointment-viewonly-satgas.html', argument)

    elif(peran == 'Dokter'):
        cursor.execute("select * from memeriksa where username_dokter='"+ email +"'")
        memeriksa = namedtuplefetchall(cursor)
        argument = {
            'memeriksa':memeriksa,
            'pesan':pesan,
        }
        cursor.execute("set search_path to public")
        cursor.close()
        return render(request, 'trigger_2/read-appointment-viewonly-dokter.html', argument)

def delete_appointment(request, nik, email_dokter, shift_praktek, tgl_praktek, kode_faskes):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("delete from memeriksa where nik_pasien='"+ nik +"' AND username_dokter='"+ email_dokter +"' AND kode_faskes='"+ kode_faskes +"' AND praktek_shift='"+ shift_praktek +"' AND praktek_tgl='"+ tgl_praktek +"'")
    # Tambahan: sehabis men-delete, jmlpasien pada jadwal_dokter juga disesuaikan
    cursor.execute("update jadwal_dokter set jmlpasien = jmlpasien-1 where username='"+ email_dokter +"' AND kode_faskes='"+ kode_faskes +"' AND shift='"+ shift_praktek +"' AND tanggal='"+ tgl_praktek +"'")
    cursor.execute("set search_path to public")
    cursor.close()
    return redirect('trigger_2:read_appointment')

def update_appointment(request, nik, email_dokter, shift_praktek, tgl_praktek, kode_faskes):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")

    if request.method == "POST":
        rekomendasi=str(request.POST['rekomendasi'])
        if(rekomendasi!=''):
            cursor.execute("update memeriksa set rekomendasi='" + rekomendasi + "' where nik_pasien='"+ nik +"' AND username_dokter='"+ email_dokter +"' AND kode_faskes='"+ kode_faskes +"' AND praktek_shift='"+ shift_praktek +"' AND praktek_tgl='"+ tgl_praktek +"'")
            cursor.execute("set search_path to public")
            cursor.close()
            return redirect('trigger_2:read_appointment')
        else:
            pesan = 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu'
            cursor.execute("set search_path to public")
            cursor.close()
            return read_appointment(request, pesan)

    cursor.execute("select * from memeriksa where nik_pasien='"+ nik +"' AND username_dokter='"+ email_dokter +"' AND kode_faskes='"+ kode_faskes +"' AND praktek_shift='"+ shift_praktek +"' AND praktek_tgl='"+ tgl_praktek +"'")
    hasil = namedtuplefetchall(cursor)
    data_awal = {
        'rekomendasi': hasil[0].rekomendasi,
    }
    form_update_rekomendasi=UpdateRekomendasi(initial = data_awal)
    argument = {    
        'updateForm' : form_update_rekomendasi,
        'nik':nik,
        'username':email_dokter,
        'shift_praktek': shift_praktek,
        'tgl_praktek': tgl_praktek,
        'kode_faskes': kode_faskes,
    }
    cursor.execute("set search_path to public")
    cursor.close()
    return render(request, 'trigger_2/update-rekomendasi.html', argument)

# Fitur nomor 6

def create_ruangan_rs(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    pesan = ''
    if request.method == "POST":
        kode_rs=request.POST.get('kode_rs')
        tipe = request.POST.get('tipe')
        harga = request.POST.get('harga')
        if tipe=='' or harga is None:
            pesan = 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu'
        else: 
            cursor.execute("select * from ruangan_rs where koders='" + kode_rs+"'")
            ruangan_rs = namedtuplefetchall(cursor)
            print(ruangan_rs)
            idx = len(ruangan_rs)+1
            if(idx < 10):
                idx_str = '0' + str(idx)
            else:
                idx_str = str(idx)
                
            kode_ruangan = kode_rs + idx_str
            str_kode_ruangan = str(kode_ruangan)
            with connection.cursor() as c:
                c.execute("""insert into SIRUCO.ruangan_rs values(%s, %s, %s, %s, %s)""",
                            [kode_rs, str_kode_ruangan, tipe, 0, harga]
                )
            return redirect('trigger_2:list_ruangan_rs')
    
    formulir = CreateRuanganRS()
    cursor.execute("select * from faskes")
    faskes = namedtuplefetchall(cursor)

    list_kode_rs = []
    for x in range (len(faskes)):
        list_kode_rs.append(faskes[x].kode)

    dict_kode_rs = {}
    
    for rs in list_kode_rs:
        list_temp = []
        cursor.execute("select koderuangan from ruangan_rs where koders='"+ rs +"'")
        list_ruangan = namedtuplefetchall(cursor)
        for ruangan in list_ruangan:
            list_temp.append(ruangan.koderuangan)
        dict_kode_rs[rs] = list_temp

    print(dict_kode_rs)

    new_kode_rs = {}
    new_char = ''
    for kode_ruang in dict_kode_rs: # kode_ruang maksudnya kode_rs
        list_ruang = dict_kode_rs[kode_ruang] #list ruangan yang mau dibandingin
        print('list')
        print(list_ruang)
        if len(list_ruang) > 9: #untuk kasus dimana substring 2 terakhir nilainya > 9 (10,11,12,13,...dst)
            list_pair_ascii = []
            for ruang in list_ruang: #perlu diubah char ke 2 terakhir supaya dapetin ascii code
                list_temp = []
                chr_3 = ruang[-2]
                chr_4 = ruang[-1]
                list_temp.append(chr_3)
                list_temp.append(chr_4) 
                list_pair_ascii.append(list_temp)

            listLAST = [0,0] #buat list yang isinya nilai 2 terakhir yang akan ditampilkan pada html
            listZ = []
            idx_one_Max = '0'
            for y in list_pair_ascii:
                if(y[0]>idx_one_Max):
                    idx_one_Max = y[0]
            
            for y in list_pair_ascii:
                if(y[0] == idx_one_Max):
                    listZ.append(y)

            idx_two_Max = '0'
            for y in listZ:
                second = y[1]
                if(second>idx_two_Max):
                    idx_two_Max = second
            
            for y in listZ:
                if(y[1] == idx_two_Max):
                    listLAST[0] = y[0]
                    listLAST[1] = y[1]
            
            if(listLAST[1]=='9'):
                listLAST[0] = ord(listLAST[0]) + 1
                listLAST[1] = 48

            else:
                listLAST[0] = ord(listLAST[0])
                listLAST[1] = ord(listLAST[1]) + 1

            new_char = chr(listLAST[0]) + chr(listLAST[1])
            new_kode_rs[kode_ruang] = kode_ruang + new_char 

        elif len(list_ruang) == 0: # untuk yang di ruangan_rs nya tuh belum ada ruangan_rs dengan kode_rs yang dipilih
            new_kode_rs[kode_ruang] = kode_ruang + '01'

        elif len(list_ruang) == 9: # untuk kasus dimana substring 2 terakhir nilainya = 9 
            new_kode_rs[kode_ruang] = kode_ruang + '10'

        else: # untuk kasus dimana substring 2 terakhir nilainya < 9
            y = str(len(list_ruang)+1)

            new_kode_rs[kode_ruang] = kode_ruang + '0' + y

    print(new_kode_rs)
    dataJSON = dumps(new_kode_rs) 
    
    argument = {
        'list_kode_rs':list_kode_rs,
        'pesan':pesan,
        'data' : dataJSON,
        'formulir':formulir,
    }

    cursor.execute("set search_path to public")
    cursor.close()
    return render(request, 'trigger_2/create-ruangan-rs.html', argument)

def list_ruangan_rs(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from ruangan_rs order by koders asc")
    ruangan_rs = namedtuplefetchall(cursor)

    argument = {
        'ruangan_rs': ruangan_rs,
    }

    cursor.execute("set search_path to public")
    cursor.close()
    return render(request, 'trigger_2/list-ruangan-rs.html', argument)

def update_ruangan_rs(request, koders, koderuangan, tipe, harga):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    data_awal = {
        'tipe':tipe,
        'harga':harga,
    }
    form_update_ruangan_rs=UpdateRuanganRS(initial = data_awal)
    if request.method == "POST":
        tipe = str(request.POST['tipe'])
        harga = request.POST['harga']
        print("harga")
        print(harga)
        if(harga is None or tipe==''):
            pesan =  "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"
            argument = {
                'pesan':pesan,
                'updateForm': form_update_ruangan_rs, 
                'koders':koders,
                'koderuangan':koderuangan,
            }
            return render(request, 'trigger_2/update-ruangan-rs.html', argument)
        else:
            with connection.cursor() as c:
                c.execute("""update SIRUCO.ruangan_rs set tipe = %s, harga = %s where koders= %s AND koderuangan= %s""",
                            [tipe, harga, koders, koderuangan]
                )
            # cursor.execute("update ruangan_rs set tipe = '"+tipe+"', harga = '"+harga+"'  where koders='"+ koders +"' AND koderuangan='"+ koderuangan+"'")
            cursor.execute("set search_path to public")
            cursor.close()
            return redirect('trigger_2:list_ruangan_rs')

    argument = {
        'updateForm': form_update_ruangan_rs, 
        'koders':koders,
        'koderuangan':koderuangan,
    }
    return render(request, 'trigger_2/update-ruangan-rs.html', argument)


def list_bed_rs(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from bed_rs order by koders asc")
    bed_rs = namedtuplefetchall(cursor)
    
    cursor.execute("select * from reservasi_rs")
    hasil = namedtuplefetchall(cursor)
    bed_rs_reserved = []
    for x in range (len(hasil)):
        bed_rs_reserved.append(hasil[x].kodebed)

    date = datetime.date.today()

    cursor.execute("select kodebed, max(tglkeluar) from reservasi_rs group by kodebed")

    hasil = namedtuplefetchall(cursor)

    dict_tglkeluar = {}

    for item in hasil:
        dict_tglkeluar[item.kodebed] = item.max

    argument = {
        'bed_rs': bed_rs,
        'date' : date,
        'bed_rs_reserved': bed_rs_reserved,
        'dict_tglkeluar': dict_tglkeluar,
    }

    cursor.execute("set search_path to public")
    cursor.close()
    return render(request, 'trigger_2/list-bed-rs.html', argument)

def delete_bed_rs(request, koderuangan, koders, kodebed):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("delete from bed_rs where koderuangan='"+ koderuangan +"' AND koders='"+ koders +"' AND kodebed='"+ kodebed +"'")
    cursor.execute("set search_path to public")
    cursor.close()
    return redirect('trigger_2:list_bed_rs')

def create_bed_rs(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    pesan = ''
    if request.method == "POST":
        kode_rs=request.POST.get('kode_rs')
        kode_room = request.POST.get('kode_room')
        if kode_room is None:
            pesan = 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu'
        else: 
            cursor.execute("select * from bed_rs where koderuangan='" + kode_room +"'")
            bed_rs = namedtuplefetchall(cursor)
            print(bed_rs)
            idx = len(bed_rs)+1
            idx_str = str(idx)
            kode_rs_split = list(kode_rs) 
            first = kode_rs_split[0]
            second = kode_rs_split[1]
            if(idx < 10):
                new_kode_bed = first + second + 'B0' + idx_str
            else:
                new_kode_bed = first + second + 'B' + idx_str

            str_kode_bed = str(new_kode_bed)
            with connection.cursor() as c:
                c.execute("""insert into SIRUCO.bed_rs values(%s, %s, %s)""",
                            [kode_room, kode_rs, str_kode_bed]
                )
            return redirect('trigger_2:list_ruangan_rs')

    cursor.execute("select * from faskes")
    rs = namedtuplefetchall(cursor)

    list_kode_rs = []
    for x in range (len(rs)):
        list_kode_rs.append(rs[x].kode)

    dict_kode_rs = {}
    
    for rs in list_kode_rs:
        list_temp = []
        cursor.execute("select koderuangan from ruangan_rs where koders='"+ rs +"'")
        list_ruangan = namedtuplefetchall(cursor)
        for ruangan in list_ruangan:
            list_temp.append(ruangan.koderuangan)
        dict_kode_rs[rs] = list_temp

    print(dict_kode_rs)
    dataJSON = dumps(dict_kode_rs) 

    cursor.execute("set search_path to public")
    cursor.close()

    argument = {
        'data' : dataJSON,
        'pesan' : pesan,
        'list_kode_rs': list_kode_rs,
    }

    return render(request, 'trigger_2/create-bed-rs.html', argument)
