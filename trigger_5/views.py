from django.shortcuts import render, reverse, redirect
from .forms import *
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.db import IntegrityError, InternalError
from collections import namedtuple
from json import dumps
import datetime

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def lihat_transaksi_makan(request):
    email = request.session['email']
    password = request.session['password']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from transaksi_makan natural join transaksi_hotel")
    lunas = namedtuplefetchall(cursor)
    cursor.execute("select * from akun_pengguna where username='"+email+"' and password='"+password+"'")
    hasil1 = namedtuplefetchall(cursor)
    peran = hasil1[0].peran
    cursor.execute("set search_path to public")
    cursor.close()
    argument = {
            'table':lunas,
        }
    if (peran == "Admin_satgas"):
        
        return render(request, 'trigger_5/lihat-transaksi-makan-satgas.html', argument)
    elif (peran == "Pengguna_publik"):
        return render(request, 'trigger_5/lihat-transaksi-makan.html', argument)

def lihat_paket_makan(request):
    email = request.session['email']
    password = request.session['password']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from paket_makan")
    hasil = namedtuplefetchall(cursor)
    cursor.execute("select * from akun_pengguna where username='"+email+"' and password='"+password+"'")
    hasil1 = namedtuplefetchall(cursor)
    peran = hasil1[0].peran
    cursor.execute("set search_path to public")
    cursor.close()
    argument = {
            'table':hasil,
        }
    if (peran == "Admin_satgas"):
        return render(request, 'trigger_5/lihat-paket-makan-satgas.html', argument)
    elif (peran == "Pengguna_publik"):
        return render(request, 'trigger_5/lihat-paket-makan.html', argument)
    elif (peran == "Admin_sistem"):
        return render(request, 'trigger_5/lihat-paket-makan-sistem.html', argument)

def lihat_hotel(request):
    email = request.session['email']
    password = request.session['password']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from hotel")
    hasil = namedtuplefetchall(cursor)
    cursor.execute("select * from akun_pengguna where username='"+email+"' and password='"+password+"'")
    hasil1 = namedtuplefetchall(cursor)
    peran = hasil1[0].peran
    cursor.execute("set search_path to public")
    cursor.close()
    argument = {
            'table':hasil,
        }
    if (peran == "Admin_satgas"):
        return render(request, 'trigger_5/lihat-hotel-satgas.html', argument)
    elif (peran == "Pengguna_publik"):
        return render(request, 'trigger_5/lihat-hotel.html', argument)
    elif (peran == "Admin_sistem"):
        return render(request, 'trigger_5/lihat-hotel-sistem.html', argument)

def lihat_transaksi_makan_detail(request,idtransaksi,idtransaksimakan):
    email = request.session['email']
    password = request.session['password']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from daftar_pesan natural join paket_makan where id_transaksi ='"+idtransaksi+"' and idtransaksimakan = '"+idtransaksimakan+"'")
    hasil = namedtuplefetchall(cursor)
    cursor.execute("select * from akun_pengguna where username='"+email+"' and password='"+password+"'")
    hasil1 = namedtuplefetchall(cursor)
    peran = hasil1[0].peran
    idtransaksi = hasil[0].id_transaksi
    idtransaksimakan = hasil[0].idtransaksimakan
    kodehotel = hasil[0].kodehotel
    cursor.execute("select * from transaksi_hotel where idtransaksi='"+idtransaksi+"'")
    hasil3 = namedtuplefetchall(cursor)
    totalbayar = hasil3[0].totalbayar
    print(hasil)
    cursor.execute("set search_path to public")
    cursor.close()
    argument = {
            'table':hasil,
            'idtransaksi':idtransaksi,
            'idtransaksimakan':idtransaksimakan,
            'kodehotel':kodehotel,
            'totalbayar':totalbayar,
        }
    if (peran == "Admin_satgas"):
        return render(request, 'trigger_5/lihat-transaksi-makan-detail.html', argument)
    elif (peran == "Pengguna_publik"):
        return render(request, 'trigger_5/lihat-transaksi-makan-detail.html', argument)

def buat_paket_makan(request):
    if request.method == "POST": 
        formulir = PaketMakanForm(request.POST) 
        kode_hotel = str(request.POST['kode_hotel'])
        kode_paket = str(request.POST['kode_paket'])
        nama_paket = str(request.POST['nama_paket'])
        harga = str(request.POST['harga'])
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("insert into paket_makan values ('"+kode_hotel+ "','" +kode_paket+"', '"+nama_paket+"', '"+harga+"')") 
        cursor.execute("set search_path to public")
        cursor.close()
        return redirect('trigger_5:lihat_paket_makan')
    else:
        formulir = PaketMakanForm()
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("select * from hotel") 
        hasil = namedtuplefetchall(cursor)
        kode_hotel = []
        for x in range (len(hasil)):
            kode_hotel.append(hasil[x].kode)
        print("==========")
        print(kode_hotel)
        cursor.execute("set search_path to public")
        cursor.close()
        argument = {    
            'form' : formulir,
            'kode_hotel' : kode_hotel,
        }
    return render(request, 'trigger_5/buat-paket-makan.html', argument)

def buat_hotel(request):
    if request.method == "POST": 
        
        
        form = BuatHotelForm(request.POST)
        if form.is_valid():
            rujukan = form.cleaned_data['rujukan']
        if rujukan == True:
            rujukan = '1'

        else:
            rujukan='0'
        print(rujukan)
        nama_hotel = str(request.POST['nama_hotel'])
        jalan = str(request.POST['jalan'])
        kelurahan = str(request.POST['kelurahan'])
        kecamatan = str(request.POST['kecamatan'])
        kabkot = str(request.POST['kabkot'])
        prov = str(request.POST['prov'])
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("select count(kode) from hotel")
        hasil1 = namedtuplefetchall(cursor)
        kode = str(hasil1[0].count)
        print(kode)
        cursor.execute("insert into hotel values ('H"+kode+ "','"+nama_hotel+ "','" +rujukan+"', '"+jalan+"', '"+kelurahan+"', '"+kecamatan+"', '"+kabkot+"', '"+prov+"')") 
        cursor.execute("set search_path to public")
        cursor.close()
        return redirect('trigger_5:lihat_hotel')
    else:
        formulir = BuatHotelForm()
        cursor = connection.cursor()
        argument = {    
            'form' : formulir,
        }
    return render(request, 'trigger_5/buat-hotel.html', argument)

def delete_paket_makan(request, kodehotel, kodepaket):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("delete from paket_makan where kodehotel='"+ kodehotel +"' AND kodepaket='"+ kodepaket +"'")
    cursor.execute("set search_path to public")
    cursor.close()
    return redirect('trigger_5:lihat_paket_makan')

def update_paket_makan(request, kodehotel, kodepaket):
    if request.method == "POST":
         
        nama_paket = str(request.POST['nama_paket']) 
        harga = str(request.POST['harga'])
        
        with connection.cursor() as c:
            c.execute("""UPDATE SIRUCO.paket_makan SET nama =  %s, harga =  %s
                             WHERE kodehotel = %s and  kodepaket = %s""",
                                [nama_paket, harga, kodehotel, kodepaket]
            )
        
            
        return HttpResponseRedirect(reverse('trigger_5:lihat_paket_makan'))

    else:
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("select * from paket_makan where kodehotel = '" +kodehotel+"' and kodepaket = '" +kodepaket+"'")
        hasil = namedtuplefetchall(cursor)
        paketmakan = hasil[0]
        cursor.execute("set search_path to public")
        cursor.close()

        data_awal = {
            'nama_paket': paketmakan[2],
            'harga': paketmakan[3],
          
        }
        form_update_paket_makan = UpdatePaketMakan(initial = data_awal)

        argument = {    
        'updatepaketmakan' : form_update_paket_makan,
        'paketmakan' : paketmakan,
        }
        return render(request, 'trigger_5/update-paket-makan.html', argument)

def delete_transaksi_makan(request, idtransaksi, idtransaksimakan):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("delete from transaksi_makan where idtransaksi='"+ idtransaksi +"' AND idtransaksimakan='"+ idtransaksimakan +"'")
    cursor.execute("set search_path to public")
    cursor.close()
    return redirect('trigger_5:lihat_transaksi_makan')

def update_hotel(request, kode):
    if request.method == "POST": 
        form = BuatHotelForm(request.POST)
        if form.is_valid():
            rujukan = form.cleaned_data['rujukan']
        if rujukan == True:
            rujukan = '1'

        else:
            rujukan='0'
            
        nama_hotel = str(request.POST['nama_hotel'])
        print('TEST')
        print(rujukan)
        jalan = str(request.POST['jalan'])
        kelurahan = str(request.POST['kelurahan'])
        kecamatan = str(request.POST['kecamatan'])
        kabkot = str(request.POST['kabkot'])
        prov = str(request.POST['prov'])
        
        with connection.cursor() as c:
            c.execute("""UPDATE SIRUCO.hotel SET nama =  %s, isrujukan = %s, jalan = %s, kelurahan = %s, kecamatan = %s, kabkot = %s, prov = %s
                             WHERE kode = %s""",
                                [nama_hotel, rujukan, jalan, kelurahan, kecamatan, kabkot, prov, kode]
            )
        
            
        return HttpResponseRedirect(reverse('trigger_5:lihat_hotel'))

    else:
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("select * from hotel where kode = '" +kode+"'")
        hasil = namedtuplefetchall(cursor)
        hotel = hasil[0]

        data_awal = {
            'nama_hotel': hotel[1],
            'jalan': hotel[3],
            'kelurahan': hotel[4],
            'kecamatan': hotel[5],
            'kabkot': hotel[6],
            'prov': hotel[7],
          
        }
        form_update_hotel = BuatHotelForm(initial = data_awal)

        argument = {    
            'updatehotel' : form_update_hotel,
            'hotel' : hotel,
        }

        cursor.execute("set search_path to public")
        cursor.close()
        return render(request, 'trigger_5/update-hotel.html', argument)
    
