from django.urls import path

from . import views

app_name = 'trigger_5'

urlpatterns = [
    path('lihat-transaksi-makan/', views.lihat_transaksi_makan, name='lihat_transaksi_makan'),
    path('lihat-paket-makan/', views.lihat_paket_makan, name='lihat_paket_makan'),
    path('lihat-hotel/', views.lihat_hotel, name='lihat_hotel'),
    path('lihat-transaksi-makan-detail/<str:idtransaksi>/<str:idtransaksimakan>', views.lihat_transaksi_makan_detail, name='lihat_transaksi_makan_detail'),
    path('buat-paket-makan/', views.buat_paket_makan, name='buat_paket_makan'),
    path('buat-hotel/', views.buat_hotel, name='buat_hotel'),
    path('delete-paket-makan/<str:kodehotel>/<str:kodepaket>', views.delete_paket_makan, name='delete_paket_makan'),
    path('update-paket-makan/<str:kodehotel>/<str:kodepaket>', views.update_paket_makan, name='update_paket_makan'),

    path('delete-transaksi-makan/<str:idtransaksi>/<str:idtransaksimakan>', views.delete_transaksi_makan, name='delete_transaksi_makan'),
    path('update-hotel/<str:kode>', views.update_hotel, name='update_hotel'),
]
