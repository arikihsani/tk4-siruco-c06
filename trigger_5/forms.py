from django import forms

class PaketMakanForm(forms.Form):
    # kode_hotel = forms.CharField(max_length=5, required=True) 
    kode_paket = forms.CharField(max_length=5, required=True)
    nama_paket = forms.CharField(max_length=20, required=True)
    harga = forms.IntegerField(required=True)

class BuatHotelForm(forms.Form):
    # kode_hotel = forms.CharField(max_length=5, required=True) 
    nama_hotel = forms.CharField(max_length=30, required=True)
    jalan = forms.CharField(max_length=30, required=True)
    kelurahan = forms.CharField(max_length=30, required=True)
    kecamatan = forms.CharField(max_length=30, required=True)
    kabkot = forms.CharField(max_length=30, required=True)
    prov = forms.CharField(max_length=30, required=True)
    rujukan = forms.BooleanField(required=False)

class UpdatePaketMakan(forms.Form):
    nama_paket = forms.CharField(max_length=20, required=True)
    harga = forms.IntegerField(required=True)