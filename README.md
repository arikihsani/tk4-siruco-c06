# Tugas Kelompok 4 (C06)

Repositori ini berisi berisi proyek Tugas Kelompok 4 yang 
dibuat oleh kelompok C06.

## Daftar Isi
- Nama Anggota Kelompok
- Link Herokuapp

## Nama Anggota Kelompok
1906298821	Daffa Aldin Wisnubroto
1906298986	Khusnul Khotimah
1906353523	Athallah Rikza Ihsani
1906398843	Salvian Athallahrif Fadhil

## Link Herokuapp

`siruco-c06.herokuapp.com/` atau [klik][link-herokuapp]


[link-herokuapp]: http://siruco-c06.herokuapp.com/