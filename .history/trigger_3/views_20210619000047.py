from django.shortcuts import render, reverse, redirect
from .forms import *
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.db import IntegrityError, InternalError
from collections import namedtuple
from json import dumps

# Create your views here.

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def create_reservasi_rs(request):
    # karena yang bisa akses url ini pada navbar hanya admin satgas jadi gak perlu bedain peran
    if request.method == "POST":
        formulir = FormReservasiRS(request.POST)

        # email = str(request.POST['email'])
        # password = str(request.POST['password'])
        nik = str(request.POST['nik'])  
        tanggal_masuk = str(request.POST['tanggal_masuk']) 
        tanggal_keluar = str(request.POST['tanggal_keluar']) 
        kode_rs = str(request.POST['kode_rs']) 
        kode_ruangan = str(request.POST['kode_ruangan']) 
        kode_bed = str(request.POST['kode_bed']) 
            
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("insert into reservasi_rs values ('"+nik+ "','" +tanggal_masuk+ "','" +tanggal_keluar+ "','" +kode_rs+ "','" +kode_ruangan+ "','" +kode_bed+ "')") 
        cursor.close()
            
        return redirect('trigger_3:list_reservasi_rs')
        

    else:
        formulir = FormReservasiRS()
        dict_kodehotel = {}
        dict_kodehotel2 = {}
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("select distinct koders from ruangan_rs") # ambil kode rs dari ruangan_rs
        kode_rs = namedtuplefetchall(cursor) #isinya semua koders pada table ruangan rs
        
        # untuk mengirim data kode ruangan ke javascript
        for kode in kode_rs:
            list_temp = []
            cursor.execute("select koderuangan from ruangan_rs where koders = '"+kode.koders+"'")
            list_koderoom = namedtuplefetchall(cursor)
            for koderoom in list_koderoom:
                list_temp.append(koderoom.koderuangan)
            
            dict_kodehotel[kode.koders] = list_temp
        
        dataJSON = dumps(dict_kodehotel)
        #########################################
        
        # untuk mengirim data kode bed ke javascript
        for kode in kode_rs:
            list_temp = []
            cursor.execute("select kodebed from bed_rs where koders = '"+kode.koders+"'")
            list_kodebed = namedtuplefetchall(cursor)
            for kodebed in list_kodebed:
                list_temp.append(kodebed.kodebed)
            
            dict_kodehotel2[kode.koders] = list_temp
        
        dataJSON2 = dumps(dict_kodehotel2)

        cursor.close()
        argument = {    
            'formRS' : formulir,
            'data':dataJSON,
            'data2':dataJSON2
        }

    return render(request, 'trigger_3/buat_reservasi_rs.html', argument)

def list_reservasi_rs(request):
    email = request.session['email']
    password = request.session['password']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from reservasi_rs")
    hasil = namedtuplefetchall(cursor)
    cursor.execute("select * from akun_pengguna where username='"+email+"' and password='"+password+"'")
    hasil1 = namedtuplefetchall(cursor)
    peran = hasil1[0].peran
    

    cursor.execute("select * from pasien where idpendaftar='"+email+"'")
    hasil2 = namedtuplefetchall(cursor)

    if hasil2[0].nik == None:
        nik = None

    else:
        nik = hasil2[0].nik

    cursor.execute("select * from reservasi_rs where kodepasien='"+nik+"'")
    hasil3 = namedtuplefetchall(cursor)

    cursor.close()
    argument = {
        'table' : hasil,
    }

    argument2 = {
        'table' : hasil3,
    }

    if (peran == "Admin_satgas"):
        return render(request, 'trigger_3/list_reservasi_rs.html', argument)
    elif (peran == "Pengguna_publik"):
        return render(request, 'trigger_3/list_reservasi_rs_publik.html', argument2)
    
