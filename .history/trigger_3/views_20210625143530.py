from django.shortcuts import render, reverse, redirect
from .forms import *
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.db import IntegrityError, InternalError
from collections import namedtuple
from json import dumps
import datetime
from .forms import *

# Create your views here.

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def create_reservasi_rs(request):
    # karena yang bisa akses url ini pada navbar hanya admin satgas jadi gak perlu bedain peran
    if request.method == "POST":
        formulir = FormReservasiRS(request.POST)

        # email = str(request.POST['email'])
        # password = str(request.POST['password'])
        nik = str(request.POST['nik_pasien'])  
        tanggal_masuk = str(request.POST['tanggal_masuk']) 
        tanggal_keluar = str(request.POST['tanggal_keluar']) 
        kode_rs = str(request.POST['id_kode_rs']) 
        kode_ruangan = str(request.POST['kode_ruangan']) 
        kode_bed = str(request.POST['kode_bed']) 
            
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("insert into reservasi_rs values ('"+nik+ "','" +tanggal_masuk+ "','" +tanggal_keluar+ "','" +kode_rs+ "','" +kode_ruangan+ "','" +kode_bed+ "')") 
        cursor.close()
            
        return redirect('trigger_3:list_reservasi_rs')
        

    else:
        formulir = FormReservasiRS()
        dict_kodehotel = {}
        dict_kodehotel2 = {}
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("select distinct koders from ruangan_rs") # ambil kode rs dari ruangan_rs
        kode_rs = namedtuplefetchall(cursor) #isinya semua koders pada table ruangan rs
        
        # untuk mengirim data kode ruangan ke javascript
        for kode in kode_rs:
            list_temp = []
            cursor.execute("select koderuangan from ruangan_rs where koders = '"+kode.koders+"'")
            list_koderoom = namedtuplefetchall(cursor)
            for koderoom in list_koderoom:
                list_temp.append(koderoom.koderuangan)
            
            dict_kodehotel[kode.koders] = list_temp
        
        dataJSON = dumps(dict_kodehotel)
        #########################################
        
        #list nik
        cursor.execute("select * from pasien") 
        hasil4 = namedtuplefetchall(cursor)
        list_nik = []
        for x in range (len(hasil4)):
            list_nik.append(hasil4[x].nik)

        #list koders
        cursor.execute("select * from rumah_sakit") 
        hasil5 = namedtuplefetchall(cursor)
        list_koders = []
        for x in range (len(hasil5)):
            list_koders.append(hasil5[x].kode_faskes)

        # untuk mengirim data kode bed ke javascript
        for kode in kode_rs:
            list_temp = []
            cursor.execute("select kodebed from bed_rs where koders = '"+kode.koders+"'")
            list_kodebed = namedtuplefetchall(cursor)
            for kodebed in list_kodebed:
                list_temp.append(kodebed.kodebed)
            
            dict_kodehotel2[kode.koders] = list_temp
        
        dataJSON2 = dumps(dict_kodehotel2)

        cursor.close()
        argument = {    
            'formRS' : formulir,
            'data':dataJSON,
            'data2':dataJSON2,
            'list_nik':list_nik,
            'list_koders':list_koders
        }

    return render(request, 'trigger_3/buat_reservasi_rs.html', argument)

def list_reservasi_rs(request):
    email = request.session['email']
    password = request.session['password']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from reservasi_rs")
    hasil = namedtuplefetchall(cursor)
    cursor.execute("select * from akun_pengguna where username='"+email+"' and password='"+password+"'")
    hasil1 = namedtuplefetchall(cursor)
    peran = hasil1[0].peran
    date = datetime.date.today()
    

    cursor.execute("select * from pasien where idpendaftar='"+email+"'")
    hasil2 = namedtuplefetchall(cursor)

    if not len(hasil2):
        nik = None
        hasil3 = None

    else:
        nik = hasil2[0].nik

        cursor.execute("select * from reservasi_rs where kodepasien='"+nik+"'")
        hasil3 = namedtuplefetchall(cursor)

    cursor.close()
    argument = {
        'table' : hasil,
        'date': date
    }
    
    argument2 = {
        'table' : hasil3,
    }

    if (peran == "Admin_satgas"):
        return render(request, 'trigger_3/list_reservasi_rs.html', argument)
    elif (peran == "Pengguna_publik"):
        return render(request, 'trigger_3/list_reservasi_rs_publik.html', argument2)

def delete_reservasi(request, kodepasien, tglmasuk):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("delete from reservasi_rs where kodepasien= '" +kodepasien+"' and tglmasuk = '"+tglmasuk+"'")
    cursor.close()
    return redirect('trigger_3:list_reservasi_rs')

def update_reservasi(request, kodepasien, tglmasuk):
    # karena yang bisa akses url ini pada navbar hanya admin satgas jadi gak perlu bedain peran
    if request.method == "POST":
        formulir = FormUpdateReservasiRS(request.POST)
        
        tanggal_keluar = str(request.POST['tanggal_keluar']) 
        
        print(tanggal_keluar)
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("update reservasi_rs set tglkeluar = '"+tanggal_keluar+"' where kodepasien ='"+kodepasien+"' and tglmasuk = '"+tglmasuk+"'") 
        cursor.close()
            
        return redirect('trigger_3:list_reservasi_rs')
        

    else:
        
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("select * from reservasi_rs where kodepasien='"+kodepasien+"' and tglmasuk = '"+tglmasuk+"'")
        hasil = namedtuplefetchall(cursor)
        tgl_keluar = hasil[0].tglkeluar
        tanggal_keluar = {
            'tanggal_keluar':tgl_keluar
        }
        print(tanggal_keluar)
        kodepasien = hasil[0].kodepasien
        tglmasuk = hasil[0].tglmasuk
        koders = hasil[0].koders
        koderuangan = hasil[0].koderuangan
        kodebed = hasil[0].kodebed
        formulir = FormUpdateReservasiRS(initial=tanggal_keluar)
        cursor.close()
        argument = {
            'form':formulir,
            'kodepasien': kodepasien,
            'tglmasuk': tglmasuk,
            'koders': koders,
            'koderuangan': koderuangan,
            'kodebed': kodebed,
            'tglkeluar': tanggal_keluar,
        }
        print(hasil)
        return render(request, 'trigger_3/update_reservasi_rs.html', argument)

def buat_faskes(request):
    # karena yang bisa akses url ini pada navbar hanya admin satgas jadi gak perlu bedain peran
    if request.method == "POST":
        formulir = FormFaskes(request.POST)
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("SELECT * FROM faskes ORDER BY kode DESC LIMIT 1")
        last_data = dictfetchall(cursor)
        last_no_kode = last_data[0]['kode']
        updated_last_no_kode = int(last_no_kode[1:])+1
        updated_last_no_kode_str = '{:01d}'.format(updated_last_no_kode)
        no_kode = 'Y' + updated_last_no_kode_str
        tipe = str(request.POST['tipe']) 
        nama_faskes = str(request.POST['nama_faskes'])
        status_kepemilikan = str(request.POST['status_kepemilikan'])
        jalan = str(request.POST['jalan'])
        kelurahan = str(request.POST['kelurahan'])
        kecamatan = str(request.POST['kecamatan'])
        kabupaten_kota = str(request.POST['kabupaten_kota'])
        provinsi = str(request.POST['provinsi'])

        
        cursor.execute("insert into faskes values ('"+no_kode+ "','" +tipe+ "','" +nama_faskes+ "','" +status_kepemilikan+ "','" +jalan+ "','" +kelurahan+ "','" +kecamatan+ "','" +kabupaten_kota+ "','" +provinsi+ "')")
        cursor.close()
            
        return redirect('trigger_3:list-faskes')
        

    else:
        
        formulir = FormFaskes()
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("SELECT * FROM faskes ORDER BY kode DESC LIMIT 1")
        last_data = dictfetchall(cursor)
        last_no_kode = last_data[0]['kode']
        updated_last_no_kode = int(last_no_kode[1:])+1
        updated_last_no_kode_str = '{:01d}'.format(updated_last_no_kode)
        no_kode = 'Y' + updated_last_no_kode_str
        argument = {
            'form':formulir,
            'kode':no_kode
        }
    
    return render(request, 'trigger_3/buat_faskes.html', argument)
    
def list_faskes(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from faskes")
    hasil = namedtuplefetchall(cursor)
    argument = {
            'table':hasil,
        }
    return render (request, 'trigger_3/list_faskes.html', argument)

def delete_faskes(request, kodefaskes):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("delete from faskes where kode= '" +kodefaskes+"'")
    cursor.close()
    return redirect('trigger_3:list-faskes')

def update_faskes(request, kodefaskes):
     # karena yang bisa akses url ini pada navbar hanya admin satgas jadi gak perlu bedain peran
    if request.method == "POST":
        formulir = FormFaskes(request.POST)
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        tipe = str(request.POST['tipe']) 
        nama_faskes = str(request.POST['nama_faskes'])
        status_kepemilikan = str(request.POST['status_kepemilikan'])
        jalan = str(request.POST['jalan'])
        kelurahan = str(request.POST['kelurahan'])
        kecamatan = str(request.POST['kecamatan'])
        kabupaten_kota = str(request.POST['kabupaten_kota'])
        provinsi = str(request.POST['provinsi'])
        cursor.execute("Update FASKES Set tipe ='" +tipe+"', nama = '" +nama_faskes+"', statusmilik = '" +status_kepemilikan+"', jalan = '" +jalan+"', kelurahan='" +kelurahan+"', kecamatan='" +kecamatan+"', kabkot='" +kabupaten_kota+"', prov='" +provinsi+"' where kode='" +kodefaskes+"'")    
        return redirect('trigger_3:list-faskes')
        

    else:
        
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("select * from faskes where kode='"+kodefaskes+"'")
        hasil = namedtuplefetchall(cursor)
        tipe = hasil[0].tipe
        nama = hasil[0].nama
        statusmilik = hasil[0].statusmilik
        jalan = hasil[0].jalan
        kelurahan = hasil[0].kelurahan
        kecamatan = hasil[0].kecamatan
        kabkot = hasil[0].kabkot
        prov = hasil[0].prov
        
        isian = {
            'tipe':tipe,
            'status_kepemilikan': statusmilik,
            'nama_faskes':nama,
            'jalan':jalan,
            'kelurahan':kelurahan,
            'kecamatan':kecamatan,
            'kabupaten_kota':kabkot,
            'provinsi':prov
        }
        formulir = FormFaskes(initial=isian)
        cursor.close()
        argument = {
            'form':formulir,
            'kode': kodefaskes,
        }
        print(hasil)
        return render(request, 'trigger_3/update_faskes.html', argument)

def detail_faskes(request, kodefaskes):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from faskes where kode='"+kodefaskes+"'")
    hasil = namedtuplefetchall(cursor)
    tipe = hasil[0].tipe
    nama = hasil[0].nama
    statusmilik = hasil[0].statusmilik
    jalan = hasil[0].jalan
    kelurahan = hasil[0].kelurahan
    kecamatan = hasil[0].kecamatan
    kabkot = hasil[0].kabkot
    prov = hasil[0].prov

    argument = {
        'kode':kodefaskes,
        'tipe':tipe,
        'nama':nama,
        'statusmilik':statusmilik,
        'jalan':jalan,
        'kelurahan':kelurahan,
        'kecamatan':kecamatan,
        'kabkot':kabkot,
        'prov':prov
    }
    return render(request, 'trigger_3/detail_faskes.html', argument)

def buat_jadwal_faskes(request):
    # karena yang bisa akses url ini pada navbar hanya admin satgas jadi gak perlu bedain peran
    if request.method == "POST":
        
        faskes = str(request.POST['faskes'])
        shift = str(request.POST['shift'])
        tanggal = str(request.POST['tanggal'])

        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("insert into jadwal values ('"+faskes+ "','" +shift+ "','" +tanggal+ "')")
        cursor.close()
            
        return redirect('trigger_3:list-jadwal-faskes')
        

    else:
        
        formulir = FormJadwalFaskes()
        cursor = connection.cursor()
        cursor.execute("select * from faskes") 
        hasil4 = namedtuplefetchall(cursor)
        list_kodefaskes = []
        for x in range (len(hasil4)):
            list_kodefaskes.append(hasil4[x].nik)
        argument = {
            'form':formulir,
        }
    
    return render(request, 'trigger_3/buat_jadwal_faskes.html', argument)

def list_jadwal_faskes(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from jadwal")
    hasil = namedtuplefetchall(cursor)
    
    argument = {
            'table':hasil,
        }
    return render (request, 'trigger_3/list_jadwal_faskes.html', argument)

def buat_rumah_sakit(request):
    # karena yang bisa akses url ini pada navbar hanya admin satgas jadi gak perlu bedain peran
    if request.method == "POST":
        form = FormRumahSakit(request.POST)
        if form.is_valid():
            faskes = str(request.POST['faskes'])
            rujukan = form.cleaned_data['rujukan']
        if rujukan == True:
            rujukan = '1'

        else:
            rujukan='0'
        print("rujukan: "+rujukan)
        

        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("insert into rumah_sakit values ('"+faskes+ "','" +rujukan+ "')")
        cursor.close()
            
        return redirect('trigger_3:list-rumah-sakit')
        

    else:
        
        formulir = FormRumahSakit()
        
        argument = {
            'form':formulir,
        }
    
    return render(request, 'trigger_3/buat_rumah_sakit.html', argument)

def list_rumah_sakit(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from rumah_sakit")
    hasil = namedtuplefetchall(cursor)
    kode = hasil[0].kode_faskes
    rujukan = hasil[0].isrujukan
    
    isian = []
    for i in range(len(hasil)):
        rujukan = hasil[i].isrujukan
        if rujukan == '0':
            rujukan = ''

        else:
            rujukan = 'checked'
        
        isian.append(rujukan)
        zipped_list = zip(hasil, isian)

    print(isian)
    
    argument = {
            'daftar':zipped_list
        }
    return render (request, 'trigger_3/list_rumah_sakit.html', argument)


