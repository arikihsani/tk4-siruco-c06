from django import forms
from django.db import connection
import datetime


cursor = connection.cursor()
cursor.execute("SELECT nik FROM SIRUCO.PASIEN")
select = cursor.fetchall()

nik_pasien = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]

class FormReservasiRS(forms.Form):
    nik = forms.CharField(widget=forms.Select(choices=nik_pasien, attrs={
        'class': 'form-control',
        'placeholder' : 'NIK Pasien',
        'required': True,
    }))
    tanggal_masuk = forms.DateField(initial=datetime.date.today)
    tanggal_keluar = forms.DateField(initial=datetime.date.today)



    


