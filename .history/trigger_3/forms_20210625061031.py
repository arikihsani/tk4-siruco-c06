from django import forms
from django.db import connection
import datetime

cursor = connection.cursor()
cursor.execute("SELECT nik FROM SIRUCO.PASIEN")
select = cursor.fetchall()

nik_pasien = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]

cursor.execute("SELECT kode_faskes FROM SIRUCO.RUMAH_SAKIT")
select = cursor.fetchall()
kode_rumah_sakit = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]

cursor.execute("SELECT kode_faskes FROM SIRUCO.RUMAH_SAKIT")
select = cursor.fetchall()
kode_ruangan = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]





class FormReservasiRS(forms.Form):
    nik = forms.CharField(widget=forms.Select(choices=nik_pasien, attrs={
        'class': 'form-control',
        'placeholder' : 'NIK Pasien',
        'required': True,
    }))
    tanggal_masuk = forms.DateField(initial=datetime.date.today)
    tanggal_keluar = forms.DateField(initial=datetime.date.today() + datetime.timedelta(days=3))
    kode_rs = forms.CharField(widget=forms.Select(choices=kode_rumah_sakit, attrs={
        'class': 'form-control',
        'placeholder' : 'Kode Rumah Sakit',
        'required': True,
    }))

    kode_ruangan = forms.CharField(widget=forms.Select(choices=kode_rumah_sakit, attrs={
        'class': 'form-control',
        'placeholder' : 'Kode Ruangan',
        'required': True,
    }))

    kode_bed = forms.CharField(max_length=5, required=True) 

class FormUpdateReservasiRS(forms.Form):
    tanggal_keluar = forms.DateField()

class FormFaskes(forms.Form):
    tipe = forms.CharField(widget=forms.Select(choices=[('RUJ', 'RUJ'), ('IDD', 'IDD'), ('KLI', 'KLI'), ('PUS', 'PUS')], attrs={
        'class': 'form-control',
        'placeholder' : 'Tipe',
        'required': True,
    }))

    nama_faskes = forms.CharField(max_length=50, required=True) 

    status_kepemilikan = forms.CharField(widget=forms.Select(choices=[('Pemerintah', 'Pemerintah'), ('Swasta', 'Swasta')], attrs={
        'class': 'form-control',
        'placeholder' : 'Status',
        'required': True,
    }))

    jalan = forms.CharField(max_length=30, required=True)
    kelurahan = forms.CharField(max_length=30, required=True)
    kecamatan = forms.CharField(max_length=30, required=True)
    kabupaten_kota = forms.CharField(max_length=30, required=True)
    provinsi = forms.CharField(max_length=30, required=True)

class FormJadwalFaskes(forms.Form):
    cursor.execute("SELECT kode FROM SIRUCO.FASKES")
    select = cursor.fetchall()
    kode_faskes = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]
    faskes = forms.CharField(widget=forms.Select(choices=kode_faskes, attrs={
        'class': 'form-control',
        'placeholder' : 'Tipe',
        'required': True,
    }))
    shift = forms.CharField(max_length=15, required=True)
    tanggal = forms.DateField()

class FormRumahSakit(forms.Form):
    cursor.execute("SELECT kode FROM SIRUCO.FASKES")
    select = cursor.fetchall()
    kode_faskes = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]
    faskes = forms.CharField(widget=forms.Select(choices=kode_faskes, attrs={
        'class': 'form-control',
        'placeholder' : 'Tipe',
        'required': True,
    }))
    rujukan = forms.BooleanField(initial=True, required=False)
print("==========")

    

    


