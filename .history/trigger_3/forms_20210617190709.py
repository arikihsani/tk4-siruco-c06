from django import forms

class FormLogin(forms.Form):
    email = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput, max_length=20)

class AdminSistemForm(forms.Form):
    email = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput, max_length=20)

class PenggunaPublikForm(forms.Form):
    email = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput, max_length=20)
    nama = forms.CharField(max_length=50)
    nik = forms.CharField(max_length=20)
    no_hp = forms.CharField(max_length=12)

class DokterForm(forms.Form):
    email = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput, max_length=20)
    no_str = forms.CharField(max_length=20)
    nama = forms.CharField(max_length=50)
    no_hp = forms.CharField(max_length=12)
    gelar_depan = forms.CharField(max_length=10)
    gelar_belakang = forms.CharField(max_length=10)
    
class AdminSatgasForm(forms.Form):
    email = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput, max_length=20)
    # kode_faskes = ....

class PendaftaranPasienForm(forms.Form):
    nik = forms.CharField(max_length=20, required=True) 
    nama = forms.CharField(max_length=50, required=True)
    no_telp = forms.CharField(max_length=20, required=True)
    no_hp = forms.CharField(max_length=12, required=True)

    jalan_ktp = forms.CharField(max_length=30, required=True) 
    kelurahan_ktp = forms.CharField(max_length=30, required=True) 
    kecamatan_ktp = forms.CharField(max_length=30, required=True) 
    kabupaten_ktp = forms.CharField(max_length=30, required=True) 
    provinsi_ktp = forms.CharField(max_length=30, required=True) 

    jalan_dom = forms.CharField(max_length=30, required=True) 
    kelurahan_dom = forms.CharField(max_length=30, required=True) 
    kecamatan_dom = forms.CharField(max_length=30, required=True)
    kabupaten_dom = forms.CharField(max_length=30, required=True) 
    provinsi_dom = forms.CharField(max_length=30, required=True) 
