from django.urls import path

from . import views

app_name = 'trigger_3'

urlpatterns = [
    path('buat-reservasi-rs/', views.create_reservasi_rs, name='buat_reservasi_rs'),
    path('list-reservasi-rs/', views.list_reservasi_rs, name='list_reservasi_rs'),
    path('delete-reservasi-rs/<str:kodepasien>/<str:tglmasuk>', views.delete_reservasi, name='delete-reservasi-rs'),
    path('update-reservasi-rs/<str:kodepasien>/<str:tglmasuk>', views.update_reservasi, name='update-reservasi-rs'),
    path('buat-faskes/', views.buat_faskes, name='buat-faskes'),
    path('list-faskes/', views.list_faskes, name='list-faskes'),
    path('delete-faskes/<str:kodefaskes>', views.delete_faskes, name='delete-faskes'),
    path('update-faskes/<str:kodefaskes>', views.update_faskes, name='update-faskes'),
    path('detail-faskes/<str:kodefaskes>', views.detail_faskes, name='update-faskes'),
]
