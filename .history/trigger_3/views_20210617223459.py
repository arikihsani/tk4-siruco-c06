from django.shortcuts import render, reverse, redirect
from .forms import *
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.db import IntegrityError, InternalError

# Create your views here.

def create_reservasi_rs(request):
    if request.method == "POST":
        formulir = AdminSistemForm(request.POST)

        email = str(request.POST['email'])
        password = str(request.POST['password'])    
        try:    
            cursor = connection.cursor()
            cursor.execute("set search_path to siruco")
            cursor.execute("insert into akun_pengguna values ('"+email+ "','" +password+ "','Admin_sistem')") 
            cursor.execute("insert into admin values ('"+email+ "')") # foreign key ke admin
            cursor.close()
            login(request,email,password)
            return redirect('trigger_1:home')
        except InternalError:
            pesan = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'
            formulir = AdminSistemForm()
            argument = {    
                'pesan' : pesan,
                'formAdminSistem' : formulir,
            }
            
            return render(request, 'trigger_3/buat_reservasi_rs.html', argument)

    else:
        formulir = FormReservasiRS()
        dict_kodehotel = {}
        cursor = connection.cursor()
        cursor.execute("select distinct koders from ruangan_rs") # ambil kode rs dari ruangan_rs
        kode_hotel = namedtuplefetchall(cursor) #isinya semua koders pada table ruangan rs
        
        for kode in kode_hotel:
            list_temp = []
            cursor.execute("select koderuangan from ruangan_rs where koders = '"+kode.kodehotel+"'")
            list_koderoom = namedtuplefetchall(cursor)
            for koderoom in list_koderoom:
                list_temp.append(koderoom.koderoom)
            
            dict_kodehotel[kode.kodehotel] = list_temp
        
        dataJSON = dumps(dict_kodehotel)
        print(dataJSON)

        argument = {    
            'formRS' : formulir,
            'data':dataJSON
        }

    return render(request, 'trigger_3/buat_reservasi_rs.html', argument)
