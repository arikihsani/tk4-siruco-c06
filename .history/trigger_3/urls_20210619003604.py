from django.urls import path

from . import views

app_name = 'trigger_3'

urlpatterns = [
    path('buat-reservasi-rs/', views.create_reservasi_rs, name='buat_reservasi_rs'),
    path('list-reservasi-rs/', views.list_reservasi_rs, name='list_reservasi_rs'),
     path('delete-reservasi-rs/<str:kodepasien>/<str:tglmasuk>', views.delete_reservasi, name='delete-reservasi-hotel'),
    
]
