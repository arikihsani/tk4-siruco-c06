from django import forms

class FormLogin(forms.Form):
    email = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput, max_length=20)

class AdminSistemForm(forms.Form):
    email = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput, max_length=20)

class PenggunaPublikForm(forms.Form):
    email = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput, max_length=20)
    nama = forms.CharField(max_length=50)
    nik = forms.CharField(max_length=20)
    no_hp = forms.CharField(max_length=12)

class DokterForm(forms.Form):
    email = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput, max_length=20)
    no_str = forms.CharField(max_length=20)
    nama = forms.CharField(max_length=50)
    no_hp = forms.CharField(max_length=12)
    gelar_depan = forms.CharField(max_length=10)
    gelar_belakang = forms.CharField(max_length=10)
    
class AdminSatgasForm(forms.Form):
    email = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput, max_length=20)
    # kode_faskes = ....

class PendaftaranPasienForm(forms.Form):
    nik = forms.CharField(max_length=20, required=False) 
    nama = forms.CharField(max_length=50, required=False)
    no_telp = forms.CharField(max_length=20, required=False)
    no_hp = forms.CharField(max_length=12, required=False)

    jalan_ktp = forms.CharField(max_length=30, required=False) 
    kelurahan_ktp = forms.CharField(max_length=30, required=False) 
    kecamatan_ktp = forms.CharField(max_length=30, required=False) 
    kabupaten_ktp = forms.CharField(max_length=30, required=False) 
    provinsi_ktp = forms.CharField(max_length=30, required=False) 

    jalan_dom = forms.CharField(max_length=30, required=False) 
    kelurahan_dom = forms.CharField(max_length=30, required=False) 
    kecamatan_dom = forms.CharField(max_length=30, required=False)
    kabupaten_dom = forms.CharField(max_length=30, required=False) 
    provinsi_dom = forms.CharField(max_length=30, required=False) 
