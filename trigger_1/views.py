from django.shortcuts import render, reverse, redirect
from .forms import *
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.db import IntegrityError, InternalError

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def role_view(request):
    return render(request, 'trigger_1/role.html')

def register_pengguna_publik_view(request, pesan=''):
    if request.method == "POST":
        formulir = PenggunaPublikForm(request.POST)
        email = str(request.POST['email'])
        password = str(request.POST['password'])
        nama = str(request.POST['nama'])
        nik = str(request.POST['nik'])
        no_hp = str(request.POST['no_hp'])

        try:
            cursor = connection.cursor()
            cursor.execute("set search_path to siruco")
            cursor.execute("insert into akun_pengguna values ('"+email+ "','" +password+ "','Pengguna_publik')")
            cursor.execute("insert into pengguna_publik values ('"+email+"','"+nik+"','"+nama+"', 'AKTIF' ,'Pengguna_publik','"+no_hp+"')")
            cursor.execute("set search_path to public")
            cursor.close()
            login(request,email,password)
            return redirect('trigger_1:home')

        except InternalError:
            print("masuk sini ga")
            pesan = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'
            formulir = PenggunaPublikForm()
            argument = {    
                'pesan' : pesan,
                'formPenggunaPublik' : formulir,
            }
            
            return render(request, 'trigger_1/register-pengguna-publik.html', argument)

    else:
        formulir = PenggunaPublikForm()

    
        argument = {
            'formPenggunaPublik' : formulir,
            }
    return render(request, 'trigger_1/register-pengguna-publik.html', argument)


def register_dokter_view(request, pesan = ''):
    if request.method == "POST":
        formulir = DokterForm(request.POST)

        email = str(request.POST['email'])
        password = str(request.POST['password'])
        no_str = str(request.POST['no_str'])
        nama = str(request.POST['nama'])
        no_hp = str(request.POST['no_hp'])
        gelar_depan = str(request.POST['gelar_depan'])
        gelar_belakang = str(request.POST['gelar_belakang'])
        
        try:
            cursor = connection.cursor()
            cursor.execute("set search_path to siruco")
            cursor.execute("insert into akun_pengguna values ('"+email+ "','" +password+ "','Dokter')") 
            cursor.execute("insert into admin values ('"+email+ "')") # foreign key ke admin
            cursor.execute("insert into dokter values ('"+email+"','"+no_str+"','"+nama+"', '"+no_hp+"' ,'"+gelar_depan+"','"+gelar_belakang+"')")
            cursor.execute("set search_path to public")
            cursor.close()
            login(request,email,password)
            return redirect('trigger_1:home')

        except InternalError:
            print("masuk sini ga")
            pesan = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'
            formulir = DokterForm()
            argument = {    
                'pesan' : pesan,
                'formDokter' : formulir,
            }
            
            return render(request, 'trigger_1/register-dokter.html', argument)

    else:
        formulir = DokterForm()

        argument = {    
            'formDokter' : formulir,
            'pesan':pesan,
        }

    return render(request, 'trigger_1/register-dokter.html', argument)

def register_admin_sistem_view(request):
    if request.method == "POST":
        formulir = AdminSistemForm(request.POST)

        email = str(request.POST['email'])
        password = str(request.POST['password'])    
        try:    
            cursor = connection.cursor()
            cursor.execute("set search_path to siruco")
            cursor.execute("insert into akun_pengguna values ('"+email+ "','" +password+ "','Admin_sistem')") 
            cursor.execute("insert into admin values ('"+email+ "')") # foreign key ke admin
            cursor.execute("set search_path to public")
            cursor.close()
            login(request,email,password)
            return redirect('trigger_1:home')
        except InternalError:
            pesan = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'
            formulir = AdminSistemForm()
            argument = {    
                'pesan' : pesan,
                'formAdminSistem' : formulir,
            }
            
            return render(request, 'trigger_1/register-admin-sistem.html', argument)

    else:
        formulir = AdminSistemForm()

        argument = {    
            'formAdminSistem' : formulir,
        }

    return render(request, 'trigger_1/register-admin-sistem.html', argument)

def register_admin_satgas_view(request):
   
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from faskes") 
    hasil = namedtuplefetchall(cursor)
    kode_faskes = []
    for x in range (len(hasil)):
        kode_faskes.append(hasil[x].kode)

    cursor.execute("set search_path to public")
    cursor.close()


    if request.method == "POST":
        formulir = AdminSatgasForm(request.POST)

        email = str(request.POST['email'])
        password = str(request.POST['password'])
        idfaskes = str(request.POST['kode_faskes'])

        try:
            cursor = connection.cursor()
            cursor.execute("set search_path to siruco")
            cursor.execute("insert into akun_pengguna values ('"+email+ "','" +password+ "','Admin_satgas')") 
            cursor.execute("insert into admin values ('"+email+ "')") # foreign key ke admin
            cursor.execute("insert into admin_satgas values ('"+email+ "','" +idfaskes+ "')") # foreign key ke admin
            cursor.execute("set search_path to public")
            cursor.close()
            login(request,email,password)
            return redirect('trigger_1:home')
            
        except InternalError:
            pesan = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'
            formulir = AdminSatgasForm()
            argument = {    
                'pesan' : pesan,
                'formAdminSatgas' : formulir,
                'kode_faskes' : kode_faskes,
            }
            
            return render(request, 'trigger_1/register-admin-satgas.html', argument)


    else:
        formulir = AdminSatgasForm()
        argument = {    
            'formAdminSatgas' : formulir,
            'kode_faskes' : kode_faskes,
        }
    
    return render(request, 'trigger_1/register-admin-satgas.html', argument)

def halaman_login_view(request, pesan=''):
    try:
        email = request.session['email']
        return login(request)
        
    except KeyError:
        formulir = FormLogin()
        # JANGAN LUPA DIAPUS
        cursor = connection.cursor()
        cursor.execute("set search_path to siruco")
        cursor.execute("select * from akun_pengguna")
        hasil = namedtuplefetchall(cursor)
        cursor.execute("set search_path to public")
        cursor.close()
        argument = {
            'formlogin' : formulir,
            'table' : hasil,
            'pesan' : pesan,
        }
        return render(request, 'trigger_1/login.html', argument)

def login(request,email=None,Password=None):
    try:
        email = request.session['email']
        password = request.session['password']
    except:
        email = request.POST.get('email', False)
        password = request.POST.get('password', False)
        
    email = str(email)
    password = str(password)
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from akun_pengguna where username='"+email+"' and password='"+password+"'")
    
    hasil = namedtuplefetchall(cursor)

    if (hasil == []):
        cursor.execute("set search_path to public")
        cursor.close()
        pesan = "Maaf Username atau Password salah"
        argument = {
            'pesan' : pesan,
        } 
        return halaman_login_view(request, argument) #nangkep error

    peran = hasil[0].peran
    print(peran)
    
    cursor.execute("set search_path to public")
    request.session['email'] = email
    request.session['password'] = password
    request.session.set_expiry(1800)
    request.session['peran'] = hasil[0].peran

    cursor.execute("set search_path to siruco")
    
    if (peran == "Admin_sistem"):
        cursor.execute("select * from akun_pengguna where username='"+email+"'")
        hasil = namedtuplefetchall(cursor)
        profil = hasil[0]
        argument = {
            'profil' : profil,
            'peran' : peran
        }
        cursor.execute("set search_path to public")
        cursor.close()
        return render(request,'profil-admin-sistem.html', argument)
        
    elif (peran == "Pengguna_publik"):
        cursor.execute("select * from pengguna_publik where username='"+email+"'")
        hasil = namedtuplefetchall(cursor)
        profil = hasil[0]
        argument = {
            'profil' : profil,
            'peran' : peran,
            'password' : password
        }
        print(argument)
        cursor.execute("set search_path to public")
        cursor.close()
        
        return render(request,'profil-pengguna-publik.html', argument)

    elif (peran == "Dokter"):
        cursor.execute("select * from dokter where username='"+email+"'")
        hasil = namedtuplefetchall(cursor)
        profil = hasil[0]
        argument = {
            'profil' : profil,
            'peran' : peran,
            'password' : password
        }
        cursor.execute("set search_path to public")
        cursor.close()
        return render(request,'profil-dokter.html', argument)

    elif (peran == "Admin_satgas"):
        cursor.execute("select * from akun_pengguna where username='"+email+"'")
        hasil = namedtuplefetchall(cursor)
        profil = hasil[0]
        cursor.execute("select * from admin_satgas where username='"+email+"'")
        hasil1 = namedtuplefetchall(cursor)
        adminsatgas = hasil1[0]
        argument = {
            'profil' : profil,
            'peran' : peran,
            'adminsatgas' : adminsatgas,
        }
        cursor.execute("set search_path to public")
        cursor.close()
        return render(request,'profil-admin-satgas.html', argument)
    
    else:
        cursor.execute("set search_path to public")
        cursor.close()

        return render(request, "trigger_1/home.html", argument)
        
def logout(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to public")
    
    request.session.flush()
    request.session.clear_expired()
    cursor.execute("set search_path to public")
    cursor.close()
    return redirect('trigger_1:halaman_login')

def pendaftaran_pasien(request):
    pesan = ''
    if request.method == "POST":
        formulir = PendaftaranPasienForm(request.POST)

        nik = str(request.POST['nik'])
        idpendaftar = request.session['email']
        nama = str(request.POST['nama'])
        no_telp = str(request.POST['no_telp'])
        no_hp = str(request.POST['no_hp'])

        jalan_ktp = str(request.POST['jalan_ktp']) 
        kelurahan_ktp = str(request.POST['kelurahan_ktp']) 
        kecamatan_ktp = str(request.POST['kecamatan_ktp']) 
        kabupaten_ktp = str(request.POST['kabupaten_ktp']) 
        provinsi_ktp = str(request.POST['provinsi_ktp']) 

        jalan_dom = str(request.POST['jalan_dom']) 
        kelurahan_dom = str(request.POST['kelurahan_dom']) 
        kecamatan_dom = str(request.POST['kecamatan_dom'])
        kabupaten_dom = str(request.POST['kabupaten_dom'])
        provinsi_dom = str(request.POST['provinsi_dom'])   
        if nik=='' or idpendaftar == '' or nama == '' or no_telp == '' or no_hp == '' or jalan_ktp == '' or kelurahan_ktp == '' or kecamatan_ktp == '' or kabupaten_ktp == '' or provinsi_ktp == '' '' or jalan_dom == '' or kelurahan_dom == '' or kecamatan_dom == '' or kabupaten_dom == '' or provinsi_dom == '':
            pesan = "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"
        else:
            cursor = connection.cursor()
            cursor.execute("set search_path to siruco")
            cursor.execute("insert into SIRUCO.pasien values ('"+nik+ "','" +idpendaftar+"', '"+nama+"', '"+jalan_ktp+"', '"+kelurahan_ktp+
            "', '"+kecamatan_ktp+"', '"+kabupaten_ktp+"', '"+provinsi_ktp+"', '"+jalan_dom+"', '"+kelurahan_dom+"', '"+kecamatan_dom+"', '"
            +kabupaten_dom+"', '"+provinsi_dom+"', '"+no_telp+"', '"+no_hp+ "')") 
            cursor.execute("set search_path to public")
            cursor.close()
        
            return redirect('trigger_1:list_pasien')
    
    pengguna_publik = request.session['email']
    formulir = PendaftaranPasienForm()
    argument = {    
        'pendaftaranPasien' : formulir,
        'pengguna_publik' : pengguna_publik,
        'pesan': pesan,
    }
    return render(request, 'trigger_1/pendaftaran-pasien.html', argument)

def list_pasien(request):
    idpendaftar = request.session['email']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from pasien where idpendaftar = '" +idpendaftar+"'")
    hasil = namedtuplefetchall(cursor)
    cursor.execute("set search_path to public")
    cursor.close()
    argument = {
        'table' : hasil,
    }
    return render(request, 'trigger_1/list-pasien.html', argument)

def update_pasien(request, nik, nama, id):
    pesan = ''
    if request.method == "POST":
        form = PendaftaranPasienForm(request.POST)
        

        no_telp = str(request.POST['no_telp'])
        no_hp = str(request.POST['no_hp'])

        jalan_ktp = str(request.POST['jalan_ktp']) 
        kelurahan_ktp = str(request.POST['kelurahan_ktp']) 
        kecamatan_ktp = str(request.POST['kecamatan_ktp']) 
        kabupaten_ktp = str(request.POST['kabupaten_ktp']) 
        provinsi_ktp = str(request.POST['provinsi_ktp']) 

        jalan_dom = str(request.POST['jalan_dom']) 
        kelurahan_dom = str(request.POST['kelurahan_dom']) 
        kecamatan_dom = str(request.POST['kecamatan_dom'])
        kabupaten_dom = str(request.POST['kabupaten_dom'])
        provinsi_dom = str(request.POST['provinsi_dom']) 
        if no_telp == '' or no_hp == '' or jalan_ktp == '' or kelurahan_ktp == '' or kecamatan_ktp == '' or kabupaten_ktp == '' or provinsi_ktp == '' '' or jalan_dom == '' or kelurahan_dom == '' or kecamatan_dom == '' or kabupaten_dom == '' or provinsi_dom == '':
            pesan = "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"
        else:
            with connection.cursor() as c:
                c.execute("""UPDATE SIRUCO.PASIEN SET ktp_jalan =  %s, ktp_kelurahan =  %s, 
                            ktp_kecamatan = %s, ktp_kabkot = %s, ktp_prov = %s, dom_jalan =  %s, dom_kelurahan =  %s, 
                            dom_kecamatan = %s, dom_kabkot = %s, dom_prov = %s, notelp = %s, nohp = %s
                            WHERE nik = %s""",
                            [jalan_ktp, kelurahan_ktp, kecamatan_ktp, kabupaten_ktp, provinsi_ktp, jalan_dom, kelurahan_dom, kecamatan_dom, kabupaten_dom, provinsi_dom, no_telp, no_hp, nik]
                )
            
            return HttpResponseRedirect(reverse('trigger_1:list_pasien'))

   
    pengguna_publik = request.session['email']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from pasien where nik = '" +nik+"'")
    hasil = namedtuplefetchall(cursor)
    pasien = hasil[0]
    cursor.execute("set search_path to public")
    cursor.close()
    print("ini data pasien")
    print(pasien)

    data_awal = {
        'nik': pasien[0],
        'nama': pasien[2],
        'no_telp': pasien[13],
        'no_hp': pasien[14],
        'jalan_ktp': pasien[3],
        'kelurahan_ktp': pasien[4],
        'kecamatan_ktp': pasien[5],
        'kabupaten_ktp': pasien[6],
        'provinsi_ktp': pasien[7],
        'jalan_dom': pasien[8],
        'kelurahan_dom': pasien[9],
        'kecamatan_dom': pasien[10],
        'kabupaten_dom': pasien[11],
        'provinsi_dom': pasien[12],
        'pesan':pesan,
    }

    form_update_pasien = PendaftaranPasienForm(initial = data_awal)
    
    argument = {    
        'pendaftaranPasien' : form_update_pasien,
        'pasien' : pasien,
        'pengguna_publik' : pengguna_publik,
        'pesan':pesan,
    }
    return render(request, 'trigger_1/update-pasien.html', argument)

def delete_pasien(request, nik):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("delete from pasien where nik = '" +nik+"'")
    cursor.execute("set search_path to public")
    cursor.close()
    return redirect('trigger_1:list_pasien')

def detail_pasien(request, nik):
    idpendaftar = request.session['email']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco")
    cursor.execute("select * from pasien where nik = '" +nik+"'")
    hasil = namedtuplefetchall(cursor)
    pasien = hasil[0]
    cursor.execute("set search_path to public")
    cursor.close()
    argument = {
        'table' : pasien,
        'idpendaftar':idpendaftar,
    }
    return render(request, 'trigger_1/detail-pasien.html', argument)
