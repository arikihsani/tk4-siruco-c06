from django.urls import path

from . import views

app_name = 'trigger_1'

urlpatterns = [
    path('role/', views.role_view, name='role'),
    path('register/admin-satgas', views.register_admin_satgas_view, name='reg_admin_satgas'),
    path('register/admin-sistem', views.register_admin_sistem_view, name='reg_admin_sistem'),
    path('register/pengguna-publik', views.register_pengguna_publik_view, name='reg_pengguna_publik'),
    path('register/dokter', views.register_dokter_view, name='reg_dokter'),
    path('login/', views.halaman_login_view, name='halaman_login'),
    path('home/', views.login, name='home'),
    path('logout/', views.logout, name = 'logout'),
    path('pendaftaran-pasien/', views.pendaftaran_pasien, name = 'pendaftaran_pasien'),
    path('list-pasien/', views.list_pasien, name = 'list_pasien'),
    path('update-pasien/<str:nik>/<str:nama>/<str:id>', views.update_pasien, name = 'update_pasien'),
    path('delete-pasien/<str:nik>', views.delete_pasien, name = 'delete_pasien'),
    path('detail-pasien/<str:nik>', views.detail_pasien, name = 'detail_pasien'),
    
]
